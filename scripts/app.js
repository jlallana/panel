(async function() {
    "use strict";



    var icon1 = "M 19.892389,16.318647 H 1.3209594 V 2.6995993 H 0.08286408 V 17.556743 H 19.892389 Z M 18.654293,4.2472184 c 0,-0.1741072 -0.135416,-0.3095239 -0.309523,-0.3095239 h -4.20759 c -0.270833,0 -0.415923,0.3288691 -0.212798,0.5319941 l 1.170387,1.170387 -4.488095,4.4880954 -2.2537203,-2.2537204 c -0.1257441,-0.125744 -0.3191965,-0.125744 -0.4449405,0 l -5.6584824,5.6584824 1.8571429,1.857143 4.0238097,-4.02381 2.2537206,2.253721 c 0.125744,0.125744 0.319196,0.125744 0.44494,0 l 6.122768,-6.1227685 1.170387,1.1703869 c 0.203125,0.203125 0.531994,0.058036 0.531994,-0.2127976 z";

    var icon2 = "m 3.3598061,17.616027 v -2.785715 h 2.7857143 v 2.785715 z m 3.404762,0 v -2.785715 h 3.0952382 v 2.785715 z m -3.404762,-3.404762 v -3.095238 h 2.7857143 v 3.095238 z m 3.404762,0 v -3.095238 h 3.0952382 v 3.095238 z M 3.3598061,10.496979 V 7.7112646 h 2.7857143 v 2.7857144 z m 7.1190479,7.119048 v -2.785715 h 3.095238 v 2.785715 z M 6.7645681,10.496979 V 7.7112646 h 3.0952382 v 2.7857144 z m 7.4285719,7.119048 v -2.785715 h 2.785714 v 2.785715 z m -3.714286,-3.404762 v -3.095238 h 3.095238 v 3.095238 z M 7.0740919,5.8541217 c 0,0.1644345 -0.1450893,0.3095238 -0.3095238,0.3095238 H 6.1455204 c -0.1644345,0 -0.3095238,-0.1450893 -0.3095238,-0.3095238 V 3.0684073 c 0,-0.1644345 0.1450893,-0.3095238 0.3095238,-0.3095238 h 0.6190477 c 0.1644345,0 0.3095238,0.1450893 0.3095238,0.3095238 z M 14.19314,14.211265 v -3.095238 h 2.785714 v 3.095238 z M 10.478854,10.496979 V 7.7112646 h 3.095238 v 2.7857144 z m 3.714286,0 V 7.7112646 h 2.785714 v 2.7857144 z m 0.309524,-4.6428573 c 0,0.1644345 -0.14509,0.3095238 -0.309524,0.3095238 h -0.619048 c -0.164434,0 -0.309524,-0.1450893 -0.309524,-0.3095238 V 3.0684073 c 0,-0.1644345 0.14509,-0.3095238 0.309524,-0.3095238 h 0.619048 c 0.164434,0 0.309524,0.1450893 0.309524,0.3095238 z M 18.216949,5.235074 c 0,-0.6770833 -0.561011,-1.2380952 -1.238095,-1.2380952 H 15.740759 V 3.0684073 c 0,-0.8511905 -0.696429,-1.5476191 -1.547619,-1.5476191 h -0.619048 c -0.85119,0 -1.547619,0.6964286 -1.547619,1.5476191 V 3.9969788 H 8.3121872 V 3.0684073 c 0,-0.8511905 -0.6964286,-1.5476191 -1.5476191,-1.5476191 H 6.1455204 c -0.8511905,0 -1.5476191,0.6964286 -1.5476191,1.5476191 V 3.9969788 H 3.3598061 c -0.6770834,0 -1.2380953,0.5610119 -1.2380953,1.2380952 v 12.380953 c 0,0.677083 0.5610119,1.238095 1.2380953,1.238095 H 16.978854 c 0.677084,0 1.238095,-0.561012 1.238095,-1.238095 z";

    var icon3 = "m 5.0411792,13.218138 c 0,-0.164435 -0.1450893,-0.309524 -0.3095238,-0.309524 H 4.1126077 c -0.1644345,0 -0.3095238,0.145089 -0.3095238,0.309524 v 0.619048 c 0,0.164434 0.1450893,0.309523 0.3095238,0.309523 h 0.6190477 c 0.1644345,0 0.3095238,-0.145089 0.3095238,-0.309523 z m 0,-2.476191 c 0,-0.164434 -0.1450893,-0.309523 -0.3095238,-0.309523 H 4.1126077 c -0.1644345,0 -0.3095238,0.145089 -0.3095238,0.309523 v 0.619048 c 0,0.164435 0.1450893,0.309524 0.3095238,0.309524 h 0.6190477 c 0.1644345,0 0.3095238,-0.145089 0.3095238,-0.309524 z m 0,-2.4761902 c 0,-0.1644345 -0.1450893,-0.3095238 -0.3095238,-0.3095238 H 4.1126077 c -0.1644345,0 -0.3095238,0.1450893 -0.3095238,0.3095238 v 0.6190477 c 0,0.1644345 0.1450893,0.3095238 0.3095238,0.3095238 h 0.6190477 c 0.1644345,0 0.3095238,-0.1450893 0.3095238,-0.3095238 z M 16.184037,13.218138 c 0,-0.164435 -0.14509,-0.309524 -0.309524,-0.309524 H 6.5887983 c -0.1644345,0 -0.3095238,0.145089 -0.3095238,0.309524 v 0.619048 c 0,0.164434 0.1450893,0.309523 0.3095238,0.309523 h 9.2857147 c 0.164434,0 0.309524,-0.145089 0.309524,-0.309523 z m 0,-2.476191 c 0,-0.164434 -0.14509,-0.309523 -0.309524,-0.309523 H 6.5887983 c -0.1644345,0 -0.3095238,0.145089 -0.3095238,0.309523 v 0.619048 c 0,0.164435 0.1450893,0.309524 0.3095238,0.309524 h 9.2857147 c 0.164434,0 0.309524,-0.145089 0.309524,-0.309524 z m 0,-2.4761902 c 0,-0.1644345 -0.14509,-0.3095238 -0.309524,-0.3095238 H 6.5887983 c -0.1644345,0 -0.3095238,0.1450893 -0.3095238,0.3095238 v 0.6190477 c 0,0.1644345 0.1450893,0.3095238 0.3095238,0.3095238 h 9.2857147 c 0.164434,0 0.309524,-0.1450893 0.309524,-0.3095238 z m 1.238095,6.8095242 c 0,0.164434 -0.145089,0.309524 -0.309524,0.309524 H 2.8745124 c -0.1644345,0 -0.3095238,-0.14509 -0.3095238,-0.309524 V 7.0276616 c 0,-0.1644346 0.1450893,-0.3095239 0.3095238,-0.3095239 H 17.112608 c 0.164435,0 0.309524,0.1450893 0.309524,0.3095239 z m 1.238095,-10.52381 c 0,-0.8511905 -0.696428,-1.5476191 -1.547619,-1.5476191 H 2.8745124 c -0.8511905,0 -1.5476191,0.6964286 -1.5476191,1.5476191 v 10.52381 c 0,0.85119 0.6964286,1.547619 1.5476191,1.547619 H 17.112608 c 0.851191,0 1.547619,-0.696429 1.547619,-1.547619 z";



    var icon4 = "m 10.030035,19.531577 q -0.3040003,0 -0.734667,-0.354667 Q 9.1687014,19.075577 8.5607014,18.518244 7.9780347,17.96091 7.1167013,17.12491 q -0.836,-0.836 -1.7986667,-1.748 Q 4.3553679,14.439577 3.4433679,13.578244 2.5567012,12.691577 1.8727012,12.03291 q -0.6586667,-0.658666 -0.91200004,-0.912 -0.78533335,-0.76 -0.78533335,-1.3426665 0,-0.2786667 0.20266667,-0.6333334 0.10133334,-0.1773333 0.30400001,-0.4053333 0.20266667,-0.228 0.53200001,-0.5573333 L 8.7380347,0.86090994 Q 8.991368,0.63290993 9.1687014,0.48090993 9.371368,0.30357659 9.4980347,0.20224326 9.7767014,0.02490992 10.030035,0.02490992 q 0.38,0 0.810666,0.40533334 0.228,0.20266667 2.254667,2.12800004 2.026667,1.9000001 5.750667,5.6240002 l 0.304,0.304 q 0.734667,0.7346667 0.734667,1.292 0,0.3040005 -0.177334,0.5826665 -0.202666,0.354667 -0.861333,1.013334 l -7.524,7.296 q -0.253334,0.253333 -0.456,0.430666 -0.177334,0.152 -0.304,0.253334 -0.253334,0.177333 -0.532,0.177333 z M 6.9900346,14.718244 H 8.3580347 V 7.2955768 L 10.410035,7.6249101 7.6740347,4.6355767 4.9380346,7.6249101 l 2.052,-0.3293333 z m 5.3960004,0 2.736,-2.989334 -2.052,0.329334 V 4.6355767 h -1.368 V 12.058244 L 9.6500347,11.72891 Z";

    var icon5 = "m 9.9747919,19.807792 q -0.304,0 -0.7346666,-0.354667 -0.1266667,-0.101333 -0.7346667,-0.658667 -0.5826667,-0.557333 -1.4440001,-1.393333 -0.836,-0.836 -1.7986667,-1.748 -0.9626667,-0.937333 -1.8746667,-1.798667 -0.8866667,-0.886666 -1.5706667,-1.545333 -0.6586667,-0.658667 -0.91200001,-0.912 -0.78533336,-0.76 -0.78533336,-1.342667 0,-0.2786666 0.20266667,-0.6333333 0.10133334,-0.1773333 0.30400001,-0.4053333 0.20266667,-0.228 0.53199999,-0.5573333 L 8.6827919,1.1371245 Q 8.9361253,0.90912453 9.1134586,0.75712453 9.3161253,0.57979119 9.4427919,0.47845786 q 0.2786667,-0.17733334 0.532,-0.17733334 0.3800001,0 0.8106671,0.40533334 0.228,0.20266667 2.254666,2.12800004 2.026667,1.9000001 5.750667,5.6240002 l 0.304,0.304 q 0.734667,0.7346667 0.734667,1.2919999 0,0.304 -0.177333,0.582667 -0.202667,0.354666 -0.861334,1.013333 l -7.524,7.296 q -0.253333,0.253334 -0.456,0.430667 -0.177333,0.152 -0.304,0.253333 -0.253333,0.177334 -0.5320001,0.177334 z m 0,-1.089334 q 0.1520001,0 0.3546671,-0.152 0.101333,-0.076 0.228,-0.202666 0.152,-0.126667 0.329333,-0.304 l 6.764,-6.713334 q 1.089333,-1.089333 1.089333,-1.292 0,-0.2533332 -0.709333,-0.9373333 L 11.469459,2.6317912 q -0.481334,-0.4813333 -0.76,-0.76 -0.278667,-0.304 -0.38,-0.3546667 -0.202667,-0.1266666 -0.3546671,-0.1266666 -0.2533333,0 -0.912,0.6586667 L 2.2987918,8.7624581 q -1.0893334,1.0893333 -1.0893334,1.2919999 0,0.228 0.7346667,0.962667 l 7.1186668,7.042667 q 0.6586667,0.658666 0.912,0.658666 z m -3.04,-3.724 V 7.5717914 L 4.8827918,7.9011247 7.6187919,4.9117913 10.354792,7.9011247 8.3027919,7.5717914 v 7.4226666 z m 5.3960001,0 -2.7360001,-2.989333 2.0520001,0.329333 V 4.9117913 h 1.368 v 7.4226667 l 2.052,-0.329333 z";

    var icon6 = "M 19.454865,19.562925 H 0.77017141 V 0.88808634 H 19.454865 Z M 19.149367,8.6142467 V 6.3082243 h -1.271269 v 2.3060224 z m 0,2.6016663 V 8.890181 h -1.271269 v 2.325732 z m 0,-7.7360152 V 1.1541658 H 1.5585551 v 2.325732 H 2.9973554 V 2.307177 h 2.8086169 v 1.1727208 z m 0,2.5819566 V 3.7361225 H 5.8256819 V 6.0519996 H 15.08919 V 4.8595693 h 2.788908 v 1.2022851 z m 0,7.7064506 v -2.296167 h -1.271269 v 2.296167 z M 15.079336,11.206058 V 8.890181 h -3.173245 v 2.315877 z m 4.070031,5.134349 v -2.325732 h -1.271269 v 2.325732 z M 15.079336,8.6142467 V 6.3082243 H 5.8059723 V 8.6142467 H 8.9397976 V 7.4907999 h 2.9662934 v 1.1234468 z m -0.0099,5.1540583 v -2.296167 h -3.16339 v 2.296167 z m 4.079886,5.154059 v -2.355297 h -1.271269 v 2.355297 z m -4.070031,-2.581957 v -2.325732 h -3.173245 v 2.325732 z M 2.9776458,6.0618544 V 3.7361225 H 1.5782647 v 2.3257319 z m 5.952297,5.1540586 V 8.890181 H 5.8256819 v 2.325732 z m 6.1395382,7.706451 v -2.355297 h -3.16339 v 2.355297 z M 2.9776458,8.6142467 V 6.3082243 H 1.5585551 v 2.3060224 z m 5.952297,5.1540583 V 11.472138 H 5.8256819 v 2.296167 z M 2.9776458,11.215913 V 8.890181 H 1.5782647 v 2.325732 z m 5.952297,5.124494 V 14.014675 H 5.8059723 v 2.325732 z M 2.9776458,13.768305 V 11.472138 H 1.5782647 v 2.296167 z m 5.952297,5.154059 V 16.567067 H 5.8256819 v 2.355297 z M 2.9776458,16.340407 V 14.014675 H 1.5585551 v 2.325732 z m 0,2.581957 V 16.567067 H 1.5782647 v 2.355297 z";

    window.on_option_anchor = function(select) {
        window.location.href = select.value;

        if (select.value != "") {
            select.style.color = "black";
        } else {
            select.style.color = "";
        }

    };



    var expensesDatabase = "https://docs.google.com/spreadsheets/d/1Xxwu6AcZphj82ZaOR2BgnQn-hUmzzWgv9AYJ8230sB4/export?format=tsv&id=1Xxwu6AcZphj82ZaOR2BgnQn-hUmzzWgv9AYJ8230sB4&gid=129355623";
    var incomeDatabase = "https://docs.google.com/spreadsheets/d/1Xxwu6AcZphj82ZaOR2BgnQn-hUmzzWgv9AYJ8230sB4/export?format=tsv&id=1Xxwu6AcZphj82ZaOR2BgnQn-hUmzzWgv9AYJ8230sB4&gid=2045348277";


    Chart.pluginService.register({
        beforeDraw: function(chart) {
            if (chart.config.options.elements.center) {




                //Get ctx from string
                var ctx = chart.chart.ctx;


                ctx.fillStyle = chart.config.options.elements.center.backgroundColor;




                //Get options from the center object in options
                var centerConfig = chart.config.options.elements.center;
                var fontStyle = centerConfig.fontStyle || 'Arial';
                var txt = centerConfig.text;
                var color = centerConfig.color || '#000';
                var sidePadding = centerConfig.sidePadding || 20;
                var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
                //Start with a base font of 30px
                ctx.font = "30px " + fontStyle;

                //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                var stringWidth = ctx.measureText(txt).width;
                var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

                // Find out how much the font can grow in width.
                var widthRatio = elementWidth / stringWidth;
                var newFontSize = Math.floor(30 * widthRatio);
                var elementHeight = (chart.innerRadius * 2);

                // Pick a new font size so it will not be larger than the height of label.
                var fontSizeToUse = Math.min(newFontSize, elementHeight);

                //Set font settings to draw it correctly.
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);


                ctx.arc(centerX, centerY, chart.innerRadius, 0, 2 * Math.PI);
                ctx.fill();



                ctx.font = fontSizeToUse + "px " + fontStyle;
                ctx.fillStyle = "black";



                //Draw text in center
                ctx.fillText(txt, centerX, centerY);




            }
        }
    });


    function format_mk(val) {
        var value = Math.abs(val);

        if (value >= 1000000) {
            value = Math.round(value / 100000) / 10 + 'M';
        } else if (value >= 1000) {
            value = Math.round(value / 1000) + 'K';
        }
        return value;
    }

    function escape_url_text(t) {
        return t.toLowerCase()
            .replace(' ', '-')
            .replace('á', 'a')
            .replace('é', 'e')
            .replace('í', 'i')
            .replace('ó', 'o')
            .replace('ú', 'u');

    }

    window.escape_url_text = escape_url_text;


    function load() {
        return new Promise(function(resolve) {
            window.addEventListener("load", function self() {
                window.removeEventListener("load", self);
                resolve();
            });
        });
    }

    function parseAmount(val) {
        if (val == '') {
            return 0;
        }


        val = val.replace(" ", "");
        val = val.replace("$", "");
        val = val.replace(".", "");
        val = val.replace(".", "");
        val = val.replace(".", "");
        val = val.replace(",", ".");
        val = val.trim();




        var retval = parseFloat(val, 10);

        if (Number.isNaN(retval)) {
            retval = 0;
        }

        return retval;

    }

    function analyze(tsv) {
        var groups = [];
        var current = [];
        groups.push(current);

        tsv.forEach(x => {
            if (x.every(y => y.trim() == '')) {
                current = [];
                groups.push(current);
            } else {
                current.push(x);
            }
        });

        var periods = groups[1][0].slice(3);


        periods = periods.slice(0, periods.indexOf("Status")).map((x, i) => ({
            id: i + 1,
            name: x
        }));


        groups = groups.slice(2);


        var idc = 1;
        var idsc = 1;
        var eid = 1;


        var categories = groups.map(g => {
            return {
                id: idc++,
                name: g[0][0],
                alias: escape_url_text(g[0][0]),
                color: g[0][1]
            };
        });

        var expenses = [];

        var subcategories = groups.map((g, i) => {
            return g.slice(1).map((sg, si) => {

                var tmp = idsc++;

                expenses.push(sg.slice(3, periods.length + 3).map(parseAmount).map((x, pi) => ({
                    id: eid++,
                    amount: x,
                    subcategory: tmp,
                    period: pi + 1
                })));

                return {
                    id: tmp,
                    name: sg[0],
                    detail: sg[1] == '' ? null : sg[1],
                    unit: sg[2] == '' ? null : parseAmount(sg[2]),
                    category: i + 1
                }
            });
        }).flat();

        return {
            periods: periods,
            categories: categories,
            subcategories: subcategories,
            expenses: expenses.flat()
        };
    }

    function tsv(data) {
        return data.split("\n").map(x => x.split("\t"));
    }

    function download(uri) {
        return new Promise(function(resolve) {
            var xhr = new XMLHttpRequest();
            xhr.open("GET", uri, true);
            xhr.addEventListener("load", function self() {
                xhr.removeEventListener("load", self);
                resolve(xhr.responseText);
            });
            xhr.send(null);
        });
    }

    function layout(title, subtitle, submenu, extend) {


        if (submenu) {
            submenu = submenu.map(s => `<a href="${s.href}">${s.text}</a>`).join("");
        } else {
            submenu = '';
        }

        if (extend) {
            var active = extend.some(s => s.active);
            extend = extend.map(s => `<option ${s.active ? ' selected="selected" ' : ''} value="${s.href}">${s.text}</option>`).join("");
            extend = `<select ${active?' style="color: black" ' : ''}  onchange="on_option_anchor(this)"><option selected disabled>Elegir mes</option>${extend}</select>`
        } else {
            extend = '';
        }
        
        
        if(title) {
            var header =  `
            
            <main>
                <canvas></canvas>
            </main>
            
            <header>
                <div>${title} ${subtitle ? (' / ' + subtitle) : ''}</div>
                <div>
                    ${submenu} ${extend}
                </div>
            </header>`;
        } else {
            var header = '<img src="images/wallpaper.png" />';
        }
        

        document.body.innerHTML = `
            
            ${header}
            <input type="checkbox" id="menu" ${title?'':' checked="checked" disabled="disabled" '}  />
            <nav>
                <label for="menu"><span>TABLERO</span></label>
                <a href="#desvio-de-planificacion/todo"><svg><path d="${icon1}"></path></svg><span>Desvío de planificación</span></a>
                <a href="#gastos-por-periodo"><svg><path d="${icon2}"></path></svg><span>Gastos por Período</span></a>
                <a href="#gastos-por-categoria/todo"><svg><path d="${icon3}"></path></svg><span>Gastos por Categoría</span></a>
                <a href="#gastos-por-periodo-por-categoria"><svg><path d="${icon4}"></path></svg><span>Gastos por periodo</span></a>
                <a href="#ingresos-por-periodo-por-categoria"><svg><path d="${icon5}"></path></svg><span>Ingresos por periodo</span></a>
                <a href="#ingresos-vs-egresos"><svg><path d="${icon6}"></path></svg><span>Ingresos/Gastos</span></a>
            </nav>
        `;

        document.querySelectorAll('a').forEach(a => a.setAttribute("current", a.href == window.location.href ? 'true' : 'false'));
    }

    function doughnutChart(data, navigate) {


        var chart = new Chart(document.querySelector('main canvas'), {
            type: 'doughnut',
            data: {
                labels: data.map(r => r.name),

                datasets: [{
                    hoverBorderColor: data.map(r => r.color),
                    data: data.map(r => r.expenses),
                    backgroundColor: data.map(r => r.color)
                }]
            },
            options: {



                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return "$" + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString();
                            return "";
                        }
                    }
                },


                elements: {
                    center: {
                        text: "TOTAL\n$" + Math.round(data.reduce((x, y) => x + y.expenses, 0)).toLocaleString(),
                        sidePadding: 20,
                        backgroundColor: "#cbcbcb"
                    }
                },

                legend: {
                    display: false
                },

                hover: {
                    onHover: function(e) {

                        if (navigate) {

                            var point = this.getElementAtEvent(e);
                            if (point.length) e.target.style.cursor = 'pointer';
                            else e.target.style.cursor = 'default';
                        }
                    }
                },

                maintainAspectRatio: false,

                plugins:

                {
                    labels: {
                        render: function(args) {
                            var item = data[args.index];

                            if (item.detail) {
                                return args.label + '\n[' + item.detail + ']\n$' + args.value.toLocaleString();
                            } else {
                                return args.label + '\n$' + args.value.toLocaleString();
                            }
                        },
                        fontColor: '#000',
                        position: 'outside'
                    }
                }



            }
        });

        if (navigate) {

            chart.canvas.addEventListener("click", function(e) {
                var active = chart.getElementsAtEvent(e);

                if (active.length == 1) {
                    location.href = location.hash + '/' + escape_url_text(active[0]._view.label);
                }
            });
        }


    }

    function overlapChart(data) {
        var c = {
            type: 'bar',
            data: {
                labels: data.map(r => r.name),
                datasets: [{
                    label: 'Gastos',
                    data: data.map(r => r.expenses),
                    backgroundColor: data.map(r => r.color),
                    xAxisID: 'expenses-axis'
                }, {
                    label: 'Unidad',
                    data: data.map(r => r.unit),
                    borderWidth: 1,
                    backgroundColor: '#f4f4f4',
                    xAxisID: 'unit-axis',
                    borderDash: [5, 10],
                    borderColor: data.map(r => r.deficit ? '#ff0000' : '#00a651')
                }],

            },
            options: {

                maintainAspectRatio: false,
                legend: {
                    display: false
                },

                plugins:

                {
                    labels: {
                        render: function(args) {



                            if (args.dataset.xAxisID == 'expenses-axis') {

                                var target = data[args.index];


                                var value = format_mk(target.detour);



                                return (target.detour > 0 ? '+' : '-') + value;
                            }
                        },
                        fontColor: data.map(x => x.detour > 0 ? 'red' : 'green'),
                        fontStyle: 'bold'
                    }
                },


                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {

                            return tooltipItem.datasetIndex ? 'Unidad' : 'Gasto';
                        }
                    }
                },


                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function(value) {
                                return format_mk(value);
                            }
                        }
                    }],
                    xAxes: [{
                        id: 'unit-axis',
                        display: true,
                        stacked: true,
                        categoryPercentage: 0.6,
                        barPercentage: 0.6,
                        visible: false,
                        ticks: {
                            display: false,
                            callback: function(value, index, values) {
                                return "$" + data[index].unit.toLocaleString() + " / " + value;
                            }
                        }

                    }, {
                        id: 'expenses-axis',
                        display: true,
                        stacked: true,
                        type: 'category',
                        categoryPercentage: 0.4,
                        barPercentage: 0.4,
                        gridLines: {
                            offsetGridLines: true
                        },
                        offset: true,
                        ticks: {
                            callback: function(value, index, values) {
                                return "$" + data[index].expenses.toLocaleString() + " / " + value;
                            }
                        }
                    }]
                }
            }
        };
        new Chart(document.querySelector('main canvas'), c);
    }


    function stackedChart(db, category) {
        if (category) {
            var datasets = db.subcategories.filter(sc => sc.category.alias == category).map(sc => ({
                lineTension: 0,
                label: sc.name,
                data: db.periods.map(p => p.expenses.filter(e => e.subcategory.id == sc.id).map(e => e.amount).reduce((a, b) => a + b)),
                backgroundColor: sc.category.color,
                borderColor: "gray"
            }));
        } else {
            var datasets = db.categories.map(c => ({
                lineTension: 0,
                label: c.name,
                data: db.periods.map(p => p.expenses.filter(e => e.subcategory.category.id == c.id).map(e => e.amount).reduce((a, b) => a + b)),
                backgroundColor: c.color,
                borderColor: "gray"
            }));
        }

        new Chart(document.querySelector('main canvas'), {
            type: 'line',
            data: {
                labels: db.periods.map(p => p.name),
                datasets: datasets
            },
            options: {
                legend: {
                    onClick: function(e, item) {
                        if(!category) {
                            window.location.href += "/" + escape_url_text(item.text);
                        }
                        return false;
                    }
                },
                bezierCurve: false,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero: true,
                            callback: function(value) {
                                return format_mk(value);
                            }
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        title: function(args, datasets) {
                            return datasets.datasets[args[0].datasetIndex].label + " - " + args[0].xLabel;
                        },
                        label: function(args, datasets) {

                            return "$" + args.yLabel.toLocaleString(undefined, {
                                minimumFractionDigits: 2
                            });
                        }
                    }
                }

            }
        });
    }


    function detourChart(period) {
        overlapChart(getExpensesPerCategories(period));
    }


    function lineChart(data) {
        new Chart(document.querySelector('main canvas'), {
            type: 'line',
            data: {
                labels: data.map(r => r.name),

                datasets: [{
                    data: data.map(r => r.expenses),
                    backgroundColor: 'transparent',
                    borderColor: '#3a7d2f',
                    pointBorderWidth: 1,
                    pointBorderColor: "#898989",
                    pointBackgroundColor: '#68e256',
                    pointRadius: 10,
                    radius: 10

                }, {
                    type: 'bar',
                    data: data.map(r => r.expenses),
                    backgroundColor: '#68e256'
                }]
            },
            options: {
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return "$" + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString();
                            return "";
                        }
                    }
                },
                maintainAspectRatio: false,
                elements: {
                    line: {
                        tension: 0
                    }
                },
                legend: false,
                layout: {
                    padding: {
                        left: 10,
                        right: 10,
                        top: 10,
                        bottom: 10
                    }
                },
                scales: {
                    xAxes: [{
                        barThickness: 11
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            callback: function(value) {
                                return format_mk(value);
                            }
                        }
                    }]
                }
            }
        });
    }

    function getCategoryNameByAlias(alias) {
        return expensesDatabase.categories.filter(x => x.alias == alias)[0].name;
    }


    function getExpensesPerCategories(period) {
        var minimal = period == 'todo' ? 0 : (period == 'este-mes' ? expensesDatabase.periods.length : expensesDatabase.periods.length - 12);


        if (period == 'todo') {
            var fromPeriod = 1;
            var toPeriod = expensesDatabase.periods.length;
        } else if (period == 'este-mes') {
            var fromPeriod = expensesDatabase.periods.length;
            var toPeriod = expensesDatabase.periods.length;
        } else if (period == 'ultimo-año') {

            var fromPeriod = expensesDatabase.periods.length - 12;

            if (fromPeriod < 1) {
                fromPeriod = 1;
            }

            var toPeriod = expensesDatabase.periods.length;
        } else {
            var fromPeriod = parseInt(period.split("-")[1]);
            var toPeriod = parseInt(period.split("-")[1]);
        }


        var periodCount = (1 + toPeriod - fromPeriod);


        var ret = expensesDatabase.categories.map(c => ({
            name: c.name,
            color: c.color,
            unit: c.subcategories.map(sc => sc.unit * periodCount).reduce((a, b) => a + b),
            expenses: c.subcategories
                .map(sc => sc.expenses.filter(e => e.period.id >= fromPeriod && e.period.id <= toPeriod)
                    .map(e => e.amount))
                .flat()
                .reduce((a, b) => a + b)
        }));

        ret.forEach(c => c.deficit = c.expenses > c.unit);
        ret.forEach(c => c.detour = c.expenses - c.unit);


        return ret;
    }



    function getExpensesPerSubcategoryByCategory(period, category) {


        var minimal = period == 'todo' ? 0 : (period == 'este-mes' ? expensesDatabase.periods.length : expensesDatabase.periods.length - 12);


        if (period == 'todo') {
            var fromPeriod = 0;
            var toPeriod = expensesDatabase.periods.length;
        } else if (period == 'este-mes') {
            var fromPeriod = expensesDatabase.periods.length;
            var toPeriod = expensesDatabase.periods.length;
        } else if (period == 'ultimo-año') {
            var fromPeriod = expensesDatabase.periods.length - 12;
            var toPeriod = expensesDatabase.periods.length;
        } else {
            var fromPeriod = period.split("-")[1];
            var toPeriod = period.split("-")[1];
        }


        return expensesDatabase.subcategories
            .filter(sc => sc.category.alias == category)
            .map(sc => ({
                name: sc.name,
                color: sc.category.color,
                detail: sc.detail,
                expenses: sc.expenses.filter(e => e.period.id >= fromPeriod && e.period.id <= toPeriod)
                    .map(e => e.amount)
                    .flat()
                    .reduce((a, b) => a + b)
            }))
    };



    function expensesPerPeriodChart() {
        lineChart(getExpensesPerPeriod());
    }

    function expensesPerCategoriesChart(period, param) {
        doughnutChart(getExpensesPerCategories(period), true);
    }



    function getExpensesPerSubcategoryByCategoryChart(period, category) {
        doughnutChart(getExpensesPerSubcategoryByCategory(period, category));
    }



    function getExpensesPerPeriod(db) {

        if (!db) {
            db = expensesDatabase;
        }

        return db.periods.map(p => ({
            name: p.name,
            expenses: p.expenses.map(e => e.amount).reduce((a, b) => a + b)
        }));;
    }


    function explode(array, func) {
        var current = [];
        var groups = [];

        groups.push(current);
        array.forEach(item => {
            if (func(item)) {

                if (current.length) {
                    groups.push(current);
                    current = [];
                }


            } else {
                current.push(item);
            }
        });

        if (current.length) {
            groups.push(current);
        }

        return groups;
    }

    function analyzeIncome(data) {

        var periods = data[0].slice(3).map((cell, index) => ({
            id: index + 1,
            name: cell,
            expenses: []
        }));


        var tmp = explode(data, x => x[0] == "").slice(2);




        var subcat_id = 1;

        var income_id = 1;

        var categories = tmp.map((entry, index) => {

            var category = {
                id: index + 1,
                name: entry[0][0],
                alias: escape_url_text(entry[0][0]),
                color: entry[0][1]
            };

            category.subcategories = entry.slice(1).map((subentry) => {

                var subcategory = {
                    id: subcat_id++,
                    name: subentry[0],
                    category: category
                };

                subcategory.expenses = subentry.slice(3).map((cell, subindex) => {

                    var income = {
                        id: income_id++,
                        amount: parseAmount(cell),
                        period: periods[subindex],
                        subcategory: subcategory
                    };

                    periods[subindex].expenses.push(income);

                    return income;
                });



                return subcategory;
            });

            return category;
        })

        var subcategories = categories.map(x => x.subcategories).flat();
        var expenses = subcategories.map(x => x.expenses).flat();

        return {
            categories: categories,
            subcategories: subcategories,
            expenses: expenses,
            periods: periods
        };
    }

    async function init() {
        await load();
        expensesDatabase = analyze(tsv(await download(expensesDatabase)));

        incomeDatabase = analyzeIncome(tsv(await download(incomeDatabase)));



        expensesDatabase.subcategories.forEach(sc => sc.category = expensesDatabase.categories.filter(c => c.id == sc.category)[0]);
        expensesDatabase.expenses.forEach(e => e.subcategory = expensesDatabase.subcategories.filter(sc => sc.id == e.subcategory)[0]);
        expensesDatabase.expenses.forEach(e => e.period = expensesDatabase.periods.filter(p => p.id == e.period)[0]);

        expensesDatabase.categories.forEach(c => c.subcategories = expensesDatabase.subcategories.filter(sc => sc.category.id == c.id));
        expensesDatabase.subcategories.forEach(sc => sc.expenses = expensesDatabase.expenses.filter(e => e.subcategory.id == sc.id));
        expensesDatabase.periods.forEach(p => p.expenses = expensesDatabase.expenses.filter(e => e.period.id == p.id));
    }


    await init();


    function compareChart() {

        var expenses = getExpensesPerPeriod(expensesDatabase);
        var incomes = getExpensesPerPeriod(incomeDatabase);


        var chart = new Chart(document.querySelector('main canvas'), {
            type: 'bar',
            data: {
                labels: ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"],
                datasets: [{
                    label: 'Ingresos',
                    data: incomes.map(x => x.expenses),
                    backgroundColor: '#fe0965'
                }, {
                    label: 'Egresos',
                    data: expenses.map(x => x.expenses),
                    backgroundColor: '#487eb4'
                }]
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    labels: {
                        fontSize: 22,
                        generateLabels: function(ctx) {
                            return ctx.data.datasets.map(ds => ({
                                text: ds.label + ": $" + ds.data.reduce((x,y) => x+y, 0).toLocaleString(undefined, {
                                minimumFractionDigits: 2
                            }),
                                fillStyle: ds.backgroundColor,
                                stokeStyle: 'transparent'
                            }));
                        }
                    }
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return "$" + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString(undefined, {
                                minimumFractionDigits: 2
                            });
                            return "";
                        }
                    }
                },
                plugins: {
                    labels: {
                        render: function(args) {
                            if (args.value == 0) {
                                return "";
                            }

                            return format_mk(args.value);
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function(value) {
                                return format_mk(value);
                            }
                        }
                    }]
                }

            }
        });

    }



    function updatePage() {

        var parts = window.location.hash.split('/');
        var app = parts[0];
        var period = parts[1] ? decodeURIComponent(parts[1]) : "";
        var param = parts[2];


        switch (app) {
            case '#gastos-por-categoria':

                if (param) {
                    layout('Gastos por categoría', getCategoryNameByAlias(param), [{
                            href: '#gastos-por-categoria/todo/' + param,
                            text: 'Todo'
                        },
                        {
                            href: '#gastos-por-categoria/ultimo-año/' + param,
                            text: 'Último año'
                        },
                        {
                            href: '#gastos-por-categoria/este-mes/' + param,
                            text: 'Este mes'
                        }
                    ], expensesDatabase.periods.map(p => ({
                        href: `#gastos-por-categoria/periodo-${p.id}/` + param,
                        text: p.name,
                        active: period.endsWith(p.id)
                    })));



                    var backlink = document.createElement("a");
                    backlink.href = window.location.hash.split("/").slice(0, 2).join("/");
                    backlink.appendChild(document.createTextNode("↶ Volver a los datos generales"));
                    document.querySelector("header > :last-child").appendChild(backlink);
                    backlink.style.color = "#3236a6";
                    backlink.style.textDecoration = "underline";

                    getExpensesPerSubcategoryByCategoryChart(period, param);
                } else {
                    layout('Gastos por categoría', null, [{
                            href: '#gastos-por-categoria/todo',
                            text: 'Todo'
                        },
                        {
                            href: '#gastos-por-categoria/ultimo-año',
                            text: 'Último año'
                        },
                        {
                            href: '#gastos-por-categoria/este-mes',
                            text: 'Este mes'
                        }
                    ], expensesDatabase.periods.map(p => ({
                        href: `#gastos-por-categoria/periodo-${p.id}`,
                        text: p.name,
                        active: window.location.hash.endsWith(p.id)
                    })));
                    expensesPerCategoriesChart(period, param);
                }
                break;
            case '#gastos-por-periodo':
                layout('Gastos por período');
                expensesPerPeriodChart();
                break;
            case '#desvio-de-planificacion':
                layout('Desvíos de planificación', null, [{
                        href: '#desvio-de-planificacion/todo',
                        text: 'Todo'
                    },
                    {
                        href: '#desvio-de-planificacion/ultimo-año',
                        text: 'Último año'
                    },
                    {
                        href: '#desvio-de-planificacion/este-mes',
                        text: 'Este mes'
                    }
                ], expensesDatabase.periods.map(p => ({
                    href: `#desvio-de-planificacion/periodo-${p.id}`,
                    text: p.name,
                    active: window.location.hash.endsWith(p.id)
                })));
                detourChart(period);
                break;
            case '#gastos-por-periodo-por-categoria':
                layout('Gastos por periodo por categoría');
                stackedChart(expensesDatabase, period);
                
                if(period) {
                       var backlink = document.createElement("a");
                    backlink.href = window.location.hash.split("/").slice(0, 1).join("/");
                    backlink.appendChild(document.createTextNode("↶ Volver a los datos generales"));
                    document.querySelector("header > :last-child").appendChild(backlink);
                    backlink.style.color = "#3236a6";
                    backlink.style.textDecoration = "underline";
                }
                
                break;

            case '#ingresos-por-periodo-por-categoria':
                layout('Ingresos por periodo por categoría');
                stackedChart(incomeDatabase, period);
                
                if(period) {
                       var backlink = document.createElement("a");
                    backlink.href = window.location.hash.split("/").slice(0, 1).join("/");
                    backlink.appendChild(document.createTextNode("↶ Volver a los datos generales"));
                    document.querySelector("header > :last-child").appendChild(backlink);
                    backlink.style.color = "#3236a6";
                    backlink.style.textDecoration = "underline";
                }
                
                
                break;
            case '#ingresos-vs-egresos':
                layout('Comparacion de ingresos con egresos');
                
                if(period) {
                       var backlink = document.createElement("a");
                    backlink.href = window.location.hash.split("/").slice(0, 1).join("/");
                    backlink.appendChild(document.createTextNode("↶ Volver a los datos generales"));
                    document.querySelector("header > :last-child").appendChild(backlink);
                    backlink.style.color = "#3236a6";
                    backlink.style.textDecoration = "underline";
                }
                
                compareChart();
                break;
            case '':
                layout();
                break;
        }
    }

    window.expensesDatabase = expensesDatabase;
    window.incomeDatabase = incomeDatabase;

    updatePage();

    var currentPage = window.location.hash;

    window.setInterval(function() {
        if (window.location.hash != currentPage) {
            currentPage = window.location.hash;
            updatePage();
        }
    }, 100);

}());
